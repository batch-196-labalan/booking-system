

const User = require("../models/User");

//import course model
const Course = require("../models/Course");

const bcrypt = require("bcrypt");
const auth = require("../auth");


//registerUser is a method and an anonymous function in an object
module.exports.registerUser = (req,res)=>{

    const hashedPw = bcrypt.hashSync(req.body.password,10);
    console.log(hashedPw);

    let newUser = new User({

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo
    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.getUserDetails = (req,res)=>{

    console.log(req.user)

    //find() will return an array of documents which matches the criteria.
    //It will return an array even if it only found 1 document.
    //User.find({_id:req.body.id})/

    //findOne() will return a single document that matched our criteria.
    //User.findOne({_id:req.body.id})

    //findById() is a mongoose method that will allow us to find a document strictly by its id.

    // User.findById(req.body.id)

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))

}
module.exports.loginUser = (req,res) => {

    console.log(req.body);

    /*
    steps for loggin in our user:

    1.find the user by its email
    2. If we found the user, we will check his password and the hashed password in our db matches.
    3. If we don't find a user, we will send a message to the client.
    4. If upon checking the found user's password is the same our input password, we will generate a "key" for the user to have authorixation to access certain feautures in our app.
    */



User.findOne({email:req.body.email})
.then(foundUser => {

    if(foundUser === null){
        return res.send({message: "No User Found."})
    } else {
        // console.log(foundUser)

        //check if the input password from req.body matches password in our foundUser document:

        //will return true

        const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

        // console.log(isPasswordCorrect);

        //If the pw is correct, we will create a KEY, a token for our user, else we will send a message:

        if(isPasswordCorrect){

            /*
            to create  a KEY:
            we have to create our own module called auth.js.

            This module will create a encoded string which contains our users details

            this encoded string is what we call a JSONWebtoken or JWT.

            This JWT can oly properly unwrapped or decoded w/ our own secret word/string.
                

            */


            // console.log("We will create a token for the user if the password is correct")

            return res.send({accessToken: auth.createAccessToken(foundUser)});

        } else {

            return res.send({message: "Incorrect Password"});
        }

    }
})


}



// s36-37 activity
//checks if email exists or not

// module.exports.getEmailDetails = (req,res)=>{
    
//     User.findOne({email:req.body.email})
//     .then(result => res.send(result))
//     .catch(error => res.send(error))

// }


//-----UPDATED----------

module.exports.getEmailDetails = (req,res)=>{
    
    User.findOne({email:req.body.email})
    .then(result => {

        if(result === null){
            return res.send(false)
        } else {
            return res.send(true)
        }

    })
    .catch(error => res.send(error))

}



//ENROLLMENT

module.exports.enroll = async (req,res) => {

    //check the id of the user who will enroll
    // console.log(req.user.id);

    //check the id of the course who want to enroll
    // console.log(req.body.courseId);

    //validate the user if they are an admin or not
    //if the user is an admin send a message to client to end the response
    //else, we will continue:

    if(req.user.isAdmin){
        return res.send({message: "Action Forbidden"});
    }

    /*
    Enrollment will come in 2 steps

    First, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in the enrollments array.

    Second, find the course where we are enrooling and update its enrollees subdocument array. We will push the userId in the enrollees array.

    Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting Javascript continue line per line.


    aync and await - async keyword is added to function to make our function asynchronous. Which means instead of JS regular behavior of running each code line by line we will be able to wait for the result of a function.


    To be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding.

    */

    //return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array.

    let isUserUpdated = await User.findById(req.user.id).then(user => {

        // console.log(user);


        //Add the courseId in an object and push that object into users enrollment
        //because we have to follow the schema of the enrollments subdocument array:

        let newEnrollment = {

            courseId: req.body.courseId //{}

        }

        //access the enrollments array from our user and the new enrollment subdocument into the enrollments array.
        user.enrollments.push(newEnrollment)



        //We must save the user document and return the value of saving our document.
        //return true if we push the subdocument successfully
        //catch and return error message if otherwise

        return user.save()
        .then(user => true)
        .catch(err => err.message)

    })

    // console.log(isUserUpdated);

    //add an if statement abd stop the process if isUserUpdated does not contain true

    if(isUserUpdated !== true){
        return res.send({message: isUserUpdated});
    }

    //find the course where we will enroll or add the user as an enrollee and return true if we were able to push the user into the enrollees array properly or send the error message instead.

    let isCourseUpdated = await Course.findById(req.body.courseId).then(course =>{

        // console.log(course);

        let enrollees = {
            userId: req.user.id
        }

        //push the enrollees
        course.enrollees.push(enrollees);

        return course.save().then(course => true).catch(err => err.message);

    })

    // console.log(isCourseUpdated);

    //if isCourseUpdated does not contain true, send the error message to the client and stop the process.–
    if(isCourseUpdated !== true){
        return res.send({message: isCourseUpdated})
    }

    if(isUserUpdated && isCourseUpdated){
        return res.send({message: "Thank you for enrolling!"})
    }

}


















