
const Course = require("../models/Course");

module.exports.getAllCourses = (req,res)=>{

    // res.send("This route will get all courses documents");

    Course.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}


module.exports.addCourse = (req,res)=>{

    // console.log(req.body);

let newCourse = new Course({
	
	name: req.body.name,
	description: req.body.description,
	price: req.body.price

  })
// console.log(newCourse);

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getActiveCourses = (req,res) => {


	Course.find({isActive: true})
	.then(result => res.send(result))
    .catch(error => res.send(error))
}


///ACTIVITY

// module.exports.getSingleCourse = (req,res)=>{

    

//    Course.findById(req.body.id)
//     .then(result => res.send(result))
//     .catch(error => res.send(error))

// }


//UPDATED

module.exports.getSingleCourse = (req,res)=>{

    console.log(req.params) //an object that contains the value captured via route params
    //the field name of the req.params indicate the name of the route params

    console.log(req.params.courseId)

    Course.findById(req.params.courseId)
    .then(result => res.send(result))
    .catch(error => res.send(error))

}




//UPDATE A SINGLE COURSE

module.exports.updateCourse = (req,res)=>{

	//check if we can get the id
	console.log(req.params.courseId);

	//chech the update that is input
	console.log(req.body);


	//findByIdandUpdate - used to update documents and has 3 arguments:
	//findByIdAndUpdate(<id>,{updates},{new:true})

	//we can create a new object to filter update details
	//fields/property that are not part of the update object will not be updated.

	let update = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))


}



//ARCHIVE A SINGLE COURSE

module.exports.archiveCourse = (req,res)=>{

	console.log(req.params.courseId);

	// console.log(req.body);

	//update the course document to inactive for "soft delete"

	let update = {

		isActive: false
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})

	.then(result => res.send(result))
	.catch(error => res.send(error))

}























