const User = require("../models/User")
const bcrypt = require("bcrypt")


//registerUser is a method and an anonymous function in an object
module.exports.registerUser = (req,res)=>{

    const hashedPw = bcrypt.hashSync(req.body.password,10);
    console.log(hashedPw);

    let newUser = new User({

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo
    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))

}


module.exports.getUserDetails = (req,res)=>{

    // console.log(req.body);
    
    User.findOne({_id:req.body.id})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}