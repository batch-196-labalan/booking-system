
const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

// console.log(userControllers); 

const auth = require("../auth");

const {verify} = auth; //destructure




router.post("/",userControllers.registerUser);

router.post("/details",verify,userControllers.getUserDetails);


router.post('/login',userControllers.loginUser);

router.post('/checkEmail',userControllers.getEmailDetails);



//USER ENROLLMENT

//courseId will come from the req.body
//userId will come from the req.user

router.post('/enroll',verify,userControllers.enroll);

module.exports = router;