/*
    To be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.

    the Router() method, will allow us to contain our routes
*/

const express = require("express");
const router = express.Router();



const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth"); //import
const { verify,verifyAdmin } = auth;



//gets all course documents whether it is active or inactive:
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

router.post('/',verify,verifyAdmin,courseControllers.addCourse);


//get all active courses for (regular, non logged-in)
router.get('/activeCourses',courseControllers.getActiveCourses);


// router.post('/getSingleCourse',courseControllers.getSingleCourse);


//UPDATED
//we can pass data in a route without the use of request body by passing a small amount of data through the url with the use of route params:

//result: http://localhost:4000/courses/getSingleCourse/62e8bac5e98bb7597195aa94

router.get('/getSingleCourse/:courseId',courseControllers.getSingleCourse);




//UPDATE A SINGLE COURSE

//Pass the id of the course we want to update via route params
//The update details will be passed via request body.

router.put("/updateCourse/:courseId",verify,verifyAdmin,courseControllers.updateCourse);



//ARCHIVE A SINGLE COURSE

//Pass the id for the course we want to update via route params
//we will directly update the course as inactive

router.delete("/archiveCourse/:courseId",verify,verifyAdmin,courseControllers.archiveCourse);



module.exports = router;



















