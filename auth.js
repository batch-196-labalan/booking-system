/*

auth.js is our own module which will contain methods to help authorize or restruct users from accessing certain features in our application

*/



const jwt = require("jsonwebtoken");


//This is the secret string which will validate or which will use to check  validity of a pw, if a token does not contain this secret string, then that token is invalid or illegitimate:

const secret = "courseBookingAPI";

/*
	JWT is a way to securely pass info from one part of a server to the frontend or other parts of our app. This will allow us to authorize our users to access or disallow access certains of our application.

	JWT is like a gift wrapping service which will encode the user's details and can only be unwrapped by jwt's own methods and if the secret is intact.

	If the JWT seemed tampered will reject the user's attempt to access a feature in our app.
*/

module.exports.createAccessToken = (userDetails) => {


	//pick only certain details from user to be included in the token.
	//pw should not be included.

	// console.log(userDetails);

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}

	console.log(data);

	//jwt.sign() will create a JWT using our data object, with our secret.
	return jwt.sign(data,secret,{});
}


module.exports.verify = (req,res,next) => {


	//verify() is going to be used as a middleWare, wherein it will be added per route to act as a gate to check if the token being passed is valid or not.

	//this will also allow us to check if the user is allowed to access the feature or not

	//we will also check the validity of the token using its secret.


	//we will pass the token with our request headers as authorization
	//request that need a token must be able to pass the token in the authorization header.
	let token = req.headers.authorization


	//if token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization headers.
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."})
	} else {

		//when passing JWT we use the bearer token authorization. This means that when JWT is passed a word "bearer" as well as space is added


		//slice () and copy the rest of the token without the word Bearer

		//slice(<startungPositin>, <endPosition>)


		// console.log(token);

		token = token.slice(7);
		console.log(token);


		//verify the validity of atoken by checking the over all length of the token and if the token contains the secret.

		jwt.verify(token,secret,function(err,decodedToken){

			console.log(decodedToken);
			// console.log(err);


		if(err){
			return res.send({

				auth: "Failed",
				message: err.message
			})
		} else {

			req.user = decodedToken;

			next();
		  }

		})
	}



}

//verifyAdmin will be used as middleware
//It has to to follow or to be added after verify()

module.exports.verifyAdmin = (req,res,next) => {


	console.log(req.user);

	if(req.user.isAdmin){

		next();
	} else {

		return res.send({

			auth: "Failed",
			message: "Action Forbidden"

		})

	}
	

}










